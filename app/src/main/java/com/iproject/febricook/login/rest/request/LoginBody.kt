package com.iproject.febricook.login.rest.request

data class LoginBody(
    var email: String = "",
    var password: String = ""
)