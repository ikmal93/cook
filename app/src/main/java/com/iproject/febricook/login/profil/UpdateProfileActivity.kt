package com.iproject.febricook.login.profil

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.iproject.febricook.R
import com.iproject.febricook.login.extension.resizeImageFromPath
import com.iproject.febricook.login.home.HomeActivity
import com.iproject.febricook.login.menu.AddFoodActivity
import com.iproject.febricook.login.rest.request.UpdateProfileBody
import com.iproject.febricook.login.rest.response.ImageResponse
import com.iproject.febricook.login.rest.response.UpdateProfileResponse
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.update_profile_activity.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class UpdateProfileActivity : AppCompatActivity() {

    companion object {
        val TAKE_PHOTO_REQUEST: Int = 2
        val PICK_PHOTO_REQUEST: Int = 1
    }


    private var currentPhotoPath: String = ""
    private var imageResponse: ImageResponse? = null
    var fileUri: Uri? = null

    private var idProfile: Int = 0
    private var updateProfileResponse: UpdateProfileResponse? = null
    private var imageUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.update_profile_activity)
        setToolbar("Update Profile")
        intent.getIntExtra("id", 0).let {
            Log.d("IKMAL", "AHAI : $it")
            idProfile = it
        }

        update_photo_btn.setOnClickListener {
            askCameraPermission()
        }

        update_gallery_btn.setOnClickListener {
            pickPhotoFromGallery()
        }

    }

    fun updateProfile(token: String, updateProfileBody: UpdateProfileBody) {
        ApiClient.provideApiService().updateProfile("Bearer $token", updateProfileBody)
            .enqueue(object : retrofit2.Callback<UpdateProfileResponse> {
                override fun onFailure(call: retrofit2.Call<UpdateProfileResponse>, t: Throwable) {
                    Toast.makeText(
                        this@UpdateProfileActivity,
                        "An error has occured, please try again",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onResponse(
                    call: retrofit2.Call<UpdateProfileResponse>,
                    response: Response<UpdateProfileResponse>
                ) {
                    updateProfileResponse = response.body()
                    if (updateProfileResponse?.data?.code == 201){
                        Toast.makeText(
                            this@UpdateProfileActivity,
                            "Update Successfull",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        finish()
                    } else {
                        Toast.makeText(
                            this@UpdateProfileActivity,
                            "An error has occured, please try again",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                }
            })
    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(update_toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = titleToolbar
        }

        update_toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        if (resultCode == Activity.RESULT_OK
            && requestCode == AddFoodActivity.TAKE_PHOTO_REQUEST
        ) {
            //photo from camera
            //display the photo on the imageview
            currentPhotoPath.resizeImageFromPath(600, 480)

            val file = File(currentPhotoPath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("images", file.name, requestFile)
            uploadImages(body)

            Log.d("IKMAL", "CAMERA : " + currentPhotoPath)
//            imageView.setImageURI(fileUri)
        } else if (resultCode == Activity.RESULT_OK
            && requestCode == AddFoodActivity.PICK_PHOTO_REQUEST
        ) {
            //photo from gallery
            fileUri = data?.data
            fileUri?.let {
                //                getRealPathFromURI(it)?.resizeImageFromPath(1024, 720)
                val file = File(getRealPathFromURI(it))
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body = MultipartBody.Part.createFormData("images", file.name, requestFile)
                Log.d("IKMAL", "Gallery : " + getRealPathFromURI(it))
                uploadImages(body)
            }


//            imageView.setImageURI(fileUri)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun uploadImages(images: MultipartBody.Part) {
        ApiClient.provideApiService().uploadImage(images)
            .enqueue(object : retrofit2.Callback<ImageResponse> {
                override fun onFailure(call: retrofit2.Call<ImageResponse>, t: Throwable) {
                    Toast.makeText(
                        this@UpdateProfileActivity,
                        "An error has occured, please try again",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onResponse(
                    call: retrofit2.Call<ImageResponse>,
                    response: Response<ImageResponse>
                ) {
                    imageResponse = response.body()
                    imageResponse?.images?.get(0)?.let {
                        imageUrl = it
                    }

                    Glide.with(this@UpdateProfileActivity)
                        .load(imageUrl)
                        .placeholder(R.drawable.ic_account)
                        .error(R.drawable.ic_account)
                        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                        .apply(RequestOptions.circleCropTransform())
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Drawable>?,
                                isFirstResource: Boolean
                            ): Boolean {
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Drawable>?,
                                dataSource: DataSource?,
                                isFirstResource: Boolean
                            ): Boolean {
                                update_profile_image.visibility = View.VISIBLE
                                return false
                            }

                        })
                        .into(update_profile_image)


                }
            })
    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(pickImageIntent, AddFoodActivity.PICK_PHOTO_REQUEST)
    }


    private fun launchCamera() {
//        val values = ContentValues(1)
//        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
//        fileUri = contentResolver
//            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                values)
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        if(intent.resolveActivity(packageManager) != null) {
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
//                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
//            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
//        }

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.iproject.febricook.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, AddFoodActivity.TAKE_PHOTO_REQUEST)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    fun askCameraPermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                    if (report.areAllPermissionsGranted()) {
                        //once permissions are granted, launch the camera
                        launchCamera()
                    } else {
                        Toast.makeText(
                            this@UpdateProfileActivity,
                            "All permissions need to be granted to take photo",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {/* ... */
                    //show alert dialog with permission options
                    AlertDialog.Builder(this@UpdateProfileActivity)
                        .setTitle(
                            "Permissions Error!"
                        )
                        .setMessage(
                            "Please allow permissions to take photo with camera"
                        )
                        .setNegativeButton(
                            android.R.string.cancel
                        ) { dialog, _ ->
                            dialog.dismiss()
                            token.cancelPermissionRequest()
                        }
                        .setPositiveButton(
                            android.R.string.ok
                        ) { dialog, _ ->
                            dialog.dismiss()
                            token.continuePermissionRequest()
                        }
                        .setOnDismissListener {
                            token.cancelPermissionRequest()
                        }
                        .show()
                }

            }).check()

    }

    private fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.save, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.save) {
            val updateProfileBody = UpdateProfileBody()
            updateProfileBody.id = idProfile
            updateProfileBody.email = update_email_tie.text.toString()
            updateProfileBody.password = ""
            updateProfileBody.name = update_name_tie.text.toString()
            updateProfileBody.avatar_url = imageUrl
            updateProfileBody.address = update_address_tie.text.toString()
            updateProfileBody.bio = update_bio_tie.text.toString()
            updateProfile(HomeActivity.preferencesHelper?.deviceToken.toString(), updateProfileBody)
        }
        return super.onOptionsItemSelected(item)

    }
}
