package com.iproject.febricook.login.rest.request

data class UpdateProfileBody(
    var address: String = "",
    var avatar_url: String = "",
    var bio: String = "",
    var email: String = "",
    var id: Int = 0,
    var name: String = "",
    var password: String = ""
)