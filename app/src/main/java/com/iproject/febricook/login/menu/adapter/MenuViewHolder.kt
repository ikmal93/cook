package com.iproject.febricook.login.menu.adapter

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.iproject.febricook.login.rest.response.FoodListResponse
import kotlinx.android.synthetic.main.menu_list.view.*

class MenuViewHolder(itemView: View, itemListener: MenuAdapter.OnItemClickListener?): RecyclerView.ViewHolder(itemView){
    private val list: CardView = itemView.menu_root_card_list
    private val image: ImageView = itemView.menu_image_list
    private val name: TextView = itemView.menu_name_text
    private val place: TextView = itemView.menu_place_text

    init {
        list.setOnClickListener {
            if (itemListener != null) {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemListener.onDetailClick(adapterPosition)
                }
            }
        }
    }

    fun bindItem(foodListResponse: FoodListResponse){
        name.text = foodListResponse.title
        Glide.with(itemView.context)
            .load(foodListResponse.images[0].name)
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    image.visibility = View.VISIBLE
                    return false
                }

            })
            .into(image)
        place.text = foodListResponse.province_name
    }
}