package com.iproject.febricook.login.home


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.iproject.febricook.R
import com.iproject.febricook.login.entity.Menu
import com.iproject.febricook.login.entity.Promo
import com.iproject.febricook.login.home.adapter.InfoSliderAdapter
import com.iproject.febricook.login.home.adapter.TopFoodAdapter
import com.iproject.febricook.login.menu.FoodDetailActivity
import com.iproject.febricook.login.rest.response.FoodListResponse
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.home_fragment.*
import retrofit2.Response
import java.util.*

class HomeFragment : Fragment() {

    private lateinit var topFoodAdapter: TopFoodAdapter
    private var menuList: MutableList<Menu>? = mutableListOf()
    private var foodList: List<FoodListResponse>? = listOf()

    private var listUrl: MutableList<Promo>? = null
    private var swipeTimer = Timer()
    private var isTimerRunning = true
    private var nextPage = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listUrl = mutableListOf()

        listUrl?.add(
            Promo(
                1,
                "https://imgcdn1.kwikku.com/images_users/images_article_list/75164-listicle-20180607125011.jpg",
                "Promo"
            )
        )
        listUrl?.add(
            Promo(
                1,
                "https://imgcdn1.kwikku.com/images_users/images_article_list/75164-listicle-20180607124855.jpg",
                "Promo"
            )
        )
        listUrl?.add(
            Promo(
                1,
                "https://imgcdn1.kwikku.com/images_users/images_article_list/75164-listicle-20180607124918.jpeg",
                "Promo"
            )
        )

        val imageAdapter = InfoSliderAdapter(requireContext(), listUrl)
        home_oa_pager.adapter = imageAdapter
        tabLayout.setupWithViewPager(home_oa_pager)
        setUpAutoSlider()

        getFoodList()
    }

    override fun onResume() {
        super.onResume()
        if (!isTimerRunning)
            setUpAutoSlider()
    }

    override fun onStop() {
        super.onStop()
        swipeTimer.cancel()
        isTimerRunning = false
    }

    private fun setUpAutoSlider() {
        val handler = Handler()
        val update = Runnable {
            nextPage = home_oa_pager.currentItem + 1
            if (nextPage == listUrl?.size) {
                nextPage = 0
            }
            home_oa_pager.setCurrentItem(nextPage, true)
        }

        swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 5000, 5000)
    }

    fun getFoodList() {
        ApiClient.provideApiService().getFood()
            .enqueue(object : retrofit2.Callback<List<FoodListResponse>> {
                override fun onFailure(call: retrofit2.Call<List<FoodListResponse>>, t: Throwable) {
                    Toast.makeText(requireContext(), "An error has occured, please try again", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: retrofit2.Call<List<FoodListResponse>>,
                    response: Response<List<FoodListResponse>>
                ) {
                    foodList = response.body()

                    home_recycler.setHasFixedSize(true)
                    home_recycler.layoutManager = GridLayoutManager(requireContext(), 4)
                    foodList?.let {
                        topFoodAdapter = TopFoodAdapter(it.toList())
                    }
                    home_recycler.adapter = topFoodAdapter

                    topFoodAdapter.setOnClickListener(object : TopFoodAdapter.OnItemClickListener {
                        override fun onDetailClick(position: Int) {
                            context?.let {
                                startActivity(
                                    Intent(it, FoodDetailActivity::class.java)
                                        .putExtra("title", foodList?.get(position)?.title)
                                        .putExtra("foodList", Gson().toJson(foodList?.get(position))))

                            }
                        }
                    })
                }
            })
    }
}
