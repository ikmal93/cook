package com.iproject.febricook.login.rest.response

data class ProfileResponse(
    val address: String,
    val avatar_url: String,
    val bio: String,
    val created_at: String,
    val email: String,
    val is_admin: Boolean,
    val id: Int,
    val name: String,
    val password: String,
    val updated_at: String
)
