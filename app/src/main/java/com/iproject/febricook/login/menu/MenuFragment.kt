package com.iproject.febricook.login.menu


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.iproject.febricook.R
import com.iproject.febricook.login.menu.adapter.MenuAdapter
import com.iproject.febricook.login.rest.response.FoodListResponse
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.menu_fragment.*
import retrofit2.Response

class MenuFragment : Fragment() {

    private lateinit var menuAdapter: MenuAdapter
    private var foodList: List<FoodListResponse>? = listOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getFoodList()

        menu_fab.setOnClickListener {
            startActivity(Intent(requireContext(), AddFoodActivity::class.java))
        }
    }

    fun getFoodList() {
        ApiClient.provideApiService().getFood()
            .enqueue(object : retrofit2.Callback<List<FoodListResponse>> {
                override fun onFailure(call: retrofit2.Call<List<FoodListResponse>>, t: Throwable) {
                    Toast.makeText(requireContext(), "An error has occured, please try again", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: retrofit2.Call<List<FoodListResponse>>,
                    response: Response<List<FoodListResponse>>
                ) {
                    foodList = response.body()

                    menu_recycler.setHasFixedSize(true)
                    menu_recycler.layoutManager = GridLayoutManager(requireContext(), 2)
                    foodList?.let {
                        menuAdapter = MenuAdapter(it.toList())
                    }
                    menu_recycler.adapter = menuAdapter

                    menuAdapter.setOnClickListener(object : MenuAdapter.OnItemClickListener {
                        override fun onDetailClick(position: Int) {
                            context?.let {
                                startActivity(
                                    Intent(it, FoodDetailActivity::class.java)
                                        .putExtra("title", foodList?.get(position)?.title)
                                        .putExtra("foodList", Gson().toJson(foodList?.get(position))))

                            }
                        }

                    })
                }
            })
    }

    override fun onResume() {
        super.onResume()
        getFoodList()
    }


}
