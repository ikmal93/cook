package com.iproject.febricook.login.profil


import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.iproject.febricook.R
import com.iproject.febricook.login.home.HomeActivity
import com.iproject.febricook.login.rest.response.ProfileResponse
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.profil_fragment.*
import retrofit2.Response

class ProfilFragment : Fragment() {

    private var profileResponse: ProfileResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.profil_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getProfile(HomeActivity.preferencesHelper?.deviceToken.toString())
    }

    fun getProfile(token: String) {
        ApiClient.provideApiService().getProfile("Bearer $token")
            .enqueue(object : retrofit2.Callback<ProfileResponse> {
                override fun onFailure(call: retrofit2.Call<ProfileResponse>, t: Throwable) {
                    Toast.makeText(requireContext(), "An error has occured, please try again", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(call: retrofit2.Call<ProfileResponse>, response: Response<ProfileResponse>) {
                    profileResponse = response.body()


                    Log.d("IKMAL", "AHAI : " + profileResponse?.is_admin)
                    profileResponse?.is_admin?.let {
                        if (it) {
                            profile_user_list.visibility = View.VISIBLE
                        } else {
                            profile_user_list.visibility = View.INVISIBLE
                        }
                    }


                    Glide.with(requireActivity())
                        .load(profileResponse?.avatar_url)
                        .placeholder(R.drawable.ic_account)
                        .error(R.drawable.ic_account)
                        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                        .apply(RequestOptions.circleCropTransform())
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Drawable>?,
                                isFirstResource: Boolean
                            ): Boolean {
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable?,
                                model: Any?,
                                target: com.bumptech.glide.request.target.Target<Drawable>?,
                                dataSource: DataSource?,
                                isFirstResource: Boolean
                            ): Boolean {
                                profil_img_profil_image.visibility = View.VISIBLE
                                return false
                            }

                        })
                        .into(profil_img_profil_image)

                    profil_name_text.text = profileResponse?.name
                    profile_email_content_text.text = profileResponse?.email
                    profile_fav_content_text.text = profileResponse?.address
                    profile_about_me_content_text.text = profileResponse?.bio

                    profile_update.setOnClickListener {
                        startActivity(
                            Intent(requireActivity(), UpdateProfileActivity::class.java)
                                .putExtra("id", profileResponse?.id)
                        )
                    }

                    profile_user_list.setOnClickListener {
                        startActivity(
                            Intent(requireActivity(), ShowUserListActivity::class.java)
                        )
                    }
                    Log.d("IKMAL", "AJA : " + profileResponse?.id)
                }
            })
    }

    override fun onResume() {
        super.onResume()
        getProfile(HomeActivity.preferencesHelper?.deviceToken.toString())
    }

}
