package com.iproject.febricook.login.menu.adapter

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.image_list.view.*

class ImageViewHolder(itemView: View, itemListener: ImageAdapter.OnItemClickListener?): RecyclerView.ViewHolder(itemView){
    private val list: ConstraintLayout = itemView.image_constraint
    private val image: ImageView = itemView.image_holder

    init {
        list.setOnClickListener {
            if (itemListener != null) {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemListener.onDetailClick(adapterPosition)
                }
            }
        }
    }

    fun bindItem(imageUrl: String){
        Glide.with(itemView.context)
            .load(imageUrl)
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    image.visibility = View.VISIBLE
                    return false
                }

            })
            .into(image)
    }
}