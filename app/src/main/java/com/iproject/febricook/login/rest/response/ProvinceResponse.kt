package com.iproject.febricook.login.rest.response

data class ProvinceResponse(
    val created_at: String,
    val id: Int,
    val name: String,
    val updated_at: String
)