package com.iproject.febricook.login.extension

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

fun String.resizeImageFromPath(targetW: Int, targetH: Int) {
    val bmOptions = BitmapFactory.Options().apply {
        inJustDecodeBounds = true
        BitmapFactory.decodeFile(this@resizeImageFromPath, this)

        // Dimensions of camera device
        val photoW: Int = outWidth
        val photoH: Int = outHeight

        // Determine how much to scale down the image
        val scaleFactor: Int = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        inJustDecodeBounds = false
        inSampleSize = scaleFactor
        inPurgeable = true
    }
    val resizeBitmap = BitmapFactory.decodeFile(this@resizeImageFromPath, bmOptions)

    val exifInterface = ExifInterface(this@resizeImageFromPath)
    val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
    val matrix = Matrix()
    when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 -> {
            matrix.setRotate(90f)
        }
        ExifInterface.ORIENTATION_ROTATE_180 -> {
            matrix.setRotate(180f)
        }
    }
    val rotatedBitmap = Bitmap.createBitmap(resizeBitmap, 0, 0, resizeBitmap.width, resizeBitmap.height, matrix, true)

    val bytes = ByteArrayOutputStream()
    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val uri = Uri.parse(this@resizeImageFromPath)
    val resizedFile = File(uri.path)
    val fos = FileOutputStream(resizedFile)
    fos.write(bytes.toByteArray())
    fos.close()
}