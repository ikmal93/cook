package com.iproject.febricook.login.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.iproject.febricook.R
import com.iproject.febricook.login.menu.MenuFragment
import com.iproject.febricook.login.pref.PreferencesHelper
import com.iproject.febricook.login.profil.ProfilFragment
import kotlinx.android.synthetic.main.home_activity.*

class HomeActivity : AppCompatActivity() {

    companion object {
        var preferencesHelper: PreferencesHelper? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
        preferencesHelper = PreferencesHelper(this)
        setToolbar("Home")

        val navView: BottomNavigationView = findViewById(R.id.home_nav_view)

        val homeFragment = HomeFragment()
        initFragment(homeFragment)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }


    private fun initFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.home_root_constraint, fragment).commit()
    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
                setToolbar("Home")
                val homeFragment = HomeFragment()
                initFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_menu -> {
                setToolbar("Food")
                val menuFragment = MenuFragment()
                initFragment(menuFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_profile -> {
                setToolbar("Profile")
                val profileFragment = ProfilFragment()
                initFragment(profileFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(home_toolbar)
        supportActionBar?.apply {
            title = titleToolbar
        }
    }
}
