package com.iproject.febricook.login.rest.response

data class ImageResponse(
    val code: Int = 0,
    val images: List<String> = mutableListOf()
)