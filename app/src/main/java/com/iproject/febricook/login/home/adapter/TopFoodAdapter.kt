package com.iproject.febricook.login.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iproject.febricook.R
import com.iproject.febricook.login.rest.response.FoodListResponse

class TopFoodAdapter(private val foodList: List<FoodListResponse>) : RecyclerView.Adapter<TopFoodViewHolder>() {
    interface OnItemClickListener {
        fun onDetailClick(position: Int)
    }

    private var itemListener: OnItemClickListener? = null

    fun setOnClickListener(listener: OnItemClickListener) {
        itemListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopFoodViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.top_food_list, parent, false)
        return TopFoodViewHolder(view, itemListener)
    }

    override fun onBindViewHolder(holder: TopFoodViewHolder, position: Int) {
        holder.bindItem(foodList[position])
    }

    override fun getItemCount(): Int {
        return if (foodList.size >= 8){
            8
        } else {
            foodList.size
        }

    }
}