package com.iproject.febricook.login.rest.response

data class FoodListResponse(
    val benefits: List<Benefit>,
    val created_at: String,
    val disadvantages: List<Disadvantage>,
    val id: Int,
    val images: List<Image>,
    val province_id: Int,
    val province_name: String,
    val title: String,
    val updated_at: String,
    val user_id: Int
)

data class Image(
    val created_at: String,
    val description: String,
    val id: Int,
    val name: String,
    val reference_id: Int,
    val reference_type: Int,
    val updated_at: String
)

data class Benefit(
    val description: String,
    val id: Int,
    val reference_id: Int,
    val reference_type: Int
)

data class Disadvantage(
    val description: String,
    val id: Int,
    val reference_id: Int,
    val reference_type: Int
)