package com.iproject.febricook.login.home.adapter

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.iproject.febricook.login.rest.response.FoodListResponse
import kotlinx.android.synthetic.main.top_food_list.view.*

class TopFoodViewHolder(itemView: View, itemListener: TopFoodAdapter.OnItemClickListener?): RecyclerView.ViewHolder(itemView){
    private val list: CardView = itemView.top_food_constraint_list
    private val image: ImageView = itemView.top_food_image_list
    private val name: TextView = itemView.top_food_name_text

    init {
        list.setOnClickListener {
            if (itemListener != null) {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemListener.onDetailClick(adapterPosition)
                }
            }
        }
    }

    fun bindItem(foodListResponse: FoodListResponse){
        name.text = foodListResponse.title
        Glide.with(itemView.context)
            .load(foodListResponse.images[0].name)
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    image.visibility = View.VISIBLE
                    return false
                }

            })
            .into(image)
    }
}