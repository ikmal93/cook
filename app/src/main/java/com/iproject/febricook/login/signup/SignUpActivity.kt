package com.iproject.febricook.login.signup

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.iproject.febricook.R
import com.iproject.febricook.login.rest.request.SignBody
import com.iproject.febricook.login.rest.response.SignResponse
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.sign_up_activity.*
import retrofit2.Response

class SignUpActivity : AppCompatActivity() {

    private var signResponse: SignResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up_activity)
        setToolbar("Sign Up")

        sign_btn.setOnClickListener {
            val signBody = SignBody()
            signBody.name = sign_name_tie.text.toString()
            signBody.email = sign_email_tie.text.toString()
            signBody.password = sign_password_tie.text.toString()
            postSignUp(signBody)
        }
    }

    fun postSignUp(signBody: SignBody) {
        ApiClient.provideApiService().signUp(signBody)
            .enqueue(object : retrofit2.Callback<SignResponse> {
                override fun onFailure(call: retrofit2.Call<SignResponse>, t: Throwable) {
                    Toast.makeText(this@SignUpActivity, "An error has occured, please try again", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: retrofit2.Call<SignResponse>, response: Response<SignResponse>) {
                    signResponse = response.body()
                    Toast.makeText(this@SignUpActivity, "Your account has been created", Toast.LENGTH_SHORT).show()
                    finish()
                }
            })
    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(sign_toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = titleToolbar
        }

        sign_toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}
