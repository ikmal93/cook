package com.iproject.febricook.login.rest.request

data class AddFoodBody(
    var benefits: List<String> = listOf(),
    var disadvantages: List<String> = listOf(),
    var images: List<String> = listOf(),
    var province_id: Int = 0,
    var title: String = ""
)