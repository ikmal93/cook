package com.iproject.febricook.login.menu

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.iproject.febricook.login.extension.resizeImageFromPath
import com.iproject.febricook.login.menu.adapter.ImageAdapter
import com.iproject.febricook.login.pref.PreferencesHelper
import com.iproject.febricook.login.rest.request.AddFoodBody
import com.iproject.febricook.login.rest.response.CreateFoodResponse
import com.iproject.febricook.login.rest.response.ImageResponse
import com.iproject.febricook.login.rest.response.ProvinceResponse
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.add_food_activity.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class AddFoodActivity : AppCompatActivity() {

    companion object {
        val TAKE_PHOTO_REQUEST: Int = 2
        val PICK_PHOTO_REQUEST: Int = 1
    }


    private var currentPhotoPath: String = ""
    private var imageResponse: ImageResponse? = null
    var fileUri: Uri? = null
    private val imageList: MutableList<String> = mutableListOf()
    private var provinces: List<ProvinceResponse>? = listOf()
    private lateinit var imageAdapter: ImageAdapter
    private var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    private val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)

//    private lateinit var addFoodBody: AddFoodBody

    private var provinceID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.iproject.febricook.R.layout.add_food_activity)
        setToolbar("Add Food")
        PreferencesHelper(this).deviceToken?.let {
            getProviceList(it)
        }


        add_food_btn_gallery.setOnClickListener {
            pickPhotoFromGallery()
        }

        add_food_btn_photo.setOnClickListener {
            //            takePictureIntent?.resolveActivity(packageManager)
//
//            contentSelectionIntent.addCategory(Intent.ACTION_PICK)
//            contentSelectionIntent.type = "image/*"
//            val mimeTypes = arrayOf("image/jpeg", "image/png")
//            contentSelectionIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
//
//            val intentArray: Array<Intent?> = if (takePictureIntent != null){
//                arrayOf(takePictureIntent)
//            } else {
//                emptyArray()
//            }
//
//            val chooserIntent = Intent(Intent.ACTION_CHOOSER)
//            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
//            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Choose Action")
//            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
//            startActivityForResult(chooserIntent, 1)

            askCameraPermission()
        }

    }

//    override fun startActivityForResult(intent: Intent?, requestCode: Int) {
//        super.startActivityForResult(intent, requestCode)
//    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        if (resultCode == Activity.RESULT_OK
            && requestCode == TAKE_PHOTO_REQUEST
        ) {
            //photo from camera
            //display the photo on the imageview
            currentPhotoPath.resizeImageFromPath(600, 480)

            val file = File(currentPhotoPath)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("images", file.name, requestFile)
            uploadImages(body)

            Log.d("IKMAL", "CAMERA : " + currentPhotoPath)
//            imageView.setImageURI(fileUri)
        } else if (resultCode == Activity.RESULT_OK
            && requestCode == PICK_PHOTO_REQUEST
        ) {
            //photo from gallery
            fileUri = data?.data
            fileUri?.let {
//                getRealPathFromURI(it)?.resizeImageFromPath(1024, 720)
                val file = File(getRealPathFromURI(it))
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body = MultipartBody.Part.createFormData("images", file.name, requestFile)
                Log.d("IKMAL", "Gallery : " + getRealPathFromURI(it))
                uploadImages(body)
            }


//            imageView.setImageURI(fileUri)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun addFood(token: String, addFoodBody: AddFoodBody) {
        ApiClient.provideApiService().addFood(token, addFoodBody)
            .enqueue(object : retrofit2.Callback<CreateFoodResponse> {
                override fun onFailure(call: retrofit2.Call<CreateFoodResponse>, t: Throwable) {
                    Toast.makeText(this@AddFoodActivity, "An error has occured, please try again", Toast.LENGTH_SHORT)
                        .show()
                    Log.d("IKMAL", t.localizedMessage)
                }

                override fun onResponse(
                    call: retrofit2.Call<CreateFoodResponse>,
                    response: Response<CreateFoodResponse>
                ) {
                    if (response.code() == 201) {
                        var createFoodResponse = CreateFoodResponse()
                        response.body()?.let {
                            createFoodResponse = it
                        }
                        Toast.makeText(this@AddFoodActivity, createFoodResponse.data?.message, Toast.LENGTH_SHORT)
                            .show()
                        finish()
                    }

                }
            })
    }


    fun getProviceList(token: String) {
        ApiClient.provideApiService().getProvinceList("Bearer $token")
            .enqueue(object : retrofit2.Callback<List<ProvinceResponse>> {
                override fun onFailure(call: retrofit2.Call<List<ProvinceResponse>>, t: Throwable) {
                    Toast.makeText(this@AddFoodActivity, "An error has occured, please try again", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: retrofit2.Call<List<ProvinceResponse>>,
                    response: Response<List<ProvinceResponse>>
                ) {
                    provinces = response.body()
                    val provinceList: MutableList<String> = mutableListOf()
                    val provinceId: MutableList<Int> = mutableListOf()
                    provinces?.let {
                        for (item in it) {
                            provinceList.add(item.name)
                            provinceId.add(item.id)
                        }
                    }

                    val arrayAdapter =
                        ArrayAdapter(this@AddFoodActivity, android.R.layout.simple_spinner_item, provinceList)
                    add_food_province.adapter = arrayAdapter

                    add_food_province.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                            Toast.makeText(this@AddFoodActivity, provinces?.get(position)?.name, Toast.LENGTH_SHORT)
//                                .show()
//                            Toast.makeText(this@AddFoodActivity, provinceId[position].toString(), Toast.LENGTH_SHORT)
//                                .show()

                            provinceID = provinceId[position]
                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {

                        }
                    }

                }
            })
    }

    fun uploadImages(images: MultipartBody.Part) {
        ApiClient.provideApiService().uploadImage(images)
            .enqueue(object : retrofit2.Callback<ImageResponse> {
                override fun onFailure(call: retrofit2.Call<ImageResponse>, t: Throwable) {
                    Toast.makeText(this@AddFoodActivity, "An error has occured, please try again", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: retrofit2.Call<ImageResponse>,
                    response: Response<ImageResponse>
                ) {
                    imageResponse = response.body()
                    imageResponse?.images?.get(0)?.let {
                        imageList.add(it)
                    }

                    Log.d("IKMAL", "UHUI : " + Gson().toJson(imageList))

                    add_food_recycler.setHasFixedSize(true)
                    add_food_recycler.layoutManager =
                        LinearLayoutManager(this@AddFoodActivity, LinearLayoutManager.HORIZONTAL, false)
                    imageList.let {
                        imageAdapter = ImageAdapter(it.toList())
                    }
                    add_food_recycler.adapter = imageAdapter

                    imageAdapter.setOnClickListener(object : ImageAdapter.OnItemClickListener {
                        override fun onDetailClick(position: Int) {
                            startActivity(Intent(this@AddFoodActivity, FoodDetailActivity::class.java))
                        }
                    })


                }
            })
    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(add_food_toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = titleToolbar
        }

        add_food_toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(com.iproject.febricook.R.menu.save, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == com.iproject.febricook.R.id.save) {

            val benefit = add_food_advantage_tie.text.toString()
            val disadvantages = add_food_disadvantage_tie.text.toString()

            val delimiter = ","

            val benefitList = benefit.split(delimiter)
            val disadvantagesList = disadvantages.split(delimiter)

            val addFoodBody = AddFoodBody()

            addFoodBody.title = add_food_name_tie.text.toString()
            addFoodBody.benefits = benefitList
            addFoodBody.disadvantages = disadvantagesList
            addFoodBody.province_id = provinceID
            addFoodBody.images = imageList

            PreferencesHelper(this).deviceToken?.let {
                PreferencesHelper(this).deviceToken?.let {
                    addFood("Bearer $it", addFoodBody)
                    Log.d("IKMAL", "HASIL : " + Gson().toJson(addFoodBody))
                }

            }
        }
        return super.onOptionsItemSelected(item)

    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(pickImageIntent, PICK_PHOTO_REQUEST)
    }


    private fun launchCamera() {
//        val values = ContentValues(1)
//        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
//        fileUri = contentResolver
//            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                values)
//        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        if(intent.resolveActivity(packageManager) != null) {
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
//            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
//                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
//            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
//        }

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.iproject.febricook.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    fun askCameraPermission() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                    if (report.areAllPermissionsGranted()) {
                        //once permissions are granted, launch the camera
                        launchCamera()
                    } else {
                        Toast.makeText(
                            this@AddFoodActivity,
                            "All permissions need to be granted to take photo",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {/* ... */
                    //show alert dialog with permission options
                    AlertDialog.Builder(this@AddFoodActivity)
                        .setTitle(
                            "Permissions Error!"
                        )
                        .setMessage(
                            "Please allow permissions to take photo with camera"
                        )
                        .setNegativeButton(
                            android.R.string.cancel
                        ) { dialog, _ ->
                            dialog.dismiss()
                            token.cancelPermissionRequest()
                        }
                        .setPositiveButton(
                            android.R.string.ok
                        ) { dialog, _ ->
                            dialog.dismiss()
                            token.continuePermissionRequest()
                        }
                        .setOnDismissListener {
                            token.cancelPermissionRequest()
                        }
                        .show()
                }

            }).check()

    }

    private fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String?
        val cursor = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }
}
