package com.iproject.febricook.login.rest.response

data class SignResponse(
    val data: Data
)

data class Data(
    val code: Int,
    val message: String
)