package com.iproject.febricook.login.entity

data class Menu(
    var id: String = "",
    var imageUrl: String = "",
    var name: String = "",
    var place: String = "",
    var description: String = ""
)