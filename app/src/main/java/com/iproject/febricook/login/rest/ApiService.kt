package com.iproject.febricook.login.rest

import com.iproject.febricook.login.rest.request.AddFoodBody
import com.iproject.febricook.login.rest.request.LoginBody
import com.iproject.febricook.login.rest.request.SignBody
import com.iproject.febricook.login.rest.request.UpdateProfileBody
import com.iproject.febricook.login.rest.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @POST("login")
    fun login(
        @Body login: LoginBody
    ): Call<LoginResponse>

    @PUT("profile")
    fun updateProfile(
        @Header("Authorization") token: String,
        @Body updateProfileBody: UpdateProfileBody
    ): Call<UpdateProfileResponse>

    @POST("register")
    fun signUp(
        @Body signBody: SignBody
    ): Call<SignResponse>

    @POST("food")
    fun addFood(
        @Header("Authorization") token: String,
        @Body addFoodBody: AddFoodBody
    ): Call<CreateFoodResponse>

    @Multipart
    @POST("image")
    fun uploadImage(@Part image: MultipartBody.Part): Call<ImageResponse>



    @GET("public/food")
    fun getFood(): Call<List<FoodListResponse>>

    @GET("users")
    fun getUsers(): Call<List<ShowUserListResponse>>

    @GET("profile")
    fun getProfile(@Header("Authorization") token: String): Call<ProfileResponse>

    @GET("province")
    fun getProvinceList(@Header("Authorization") token: String): Call<List<ProvinceResponse>>
}