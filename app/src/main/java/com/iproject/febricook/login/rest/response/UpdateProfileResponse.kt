package com.iproject.febricook.login.rest.response

data class UpdateProfileResponse(
    val data: UpdateProfileData
)

data class UpdateProfileData(
    val code: Int,
    val message: String
)