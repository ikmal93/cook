package id.co.wmotion.savioryrots.service

import com.iproject.febricook.BuildConfig
import com.iproject.febricook.login.rest.ApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    companion object {

        private fun getOkHttpClient(): OkHttpClient {
            val logger = HttpLoggingInterceptor()

            if (BuildConfig.DEBUG)
                logger.level = HttpLoggingInterceptor.Level.BODY
            else
                logger.level = HttpLoggingInterceptor.Level.NONE

            return OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
        }

        private fun provideRetrofit(): Retrofit {
            val httpClient = getOkHttpClient()
            return Retrofit.Builder()
                    .baseUrl("http://206.189.94.143:8090/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
        }

        fun provideApiService(): ApiService {
            return provideRetrofit()
                    .create(ApiService::class.java)
        }
    }
}