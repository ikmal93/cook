package com.iproject.febricook.login.rest.response

data class CreateFoodResponse(
    var data: CreateFood? = null
)

data class CreateFood(
    var code: Int = 0,
    var message: String = ""
)