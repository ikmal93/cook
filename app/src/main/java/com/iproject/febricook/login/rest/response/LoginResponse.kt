package com.iproject.febricook.login.rest.response

data class LoginResponse(
    val code: Int,
    val token: String
)