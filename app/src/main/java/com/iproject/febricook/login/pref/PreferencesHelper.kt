package com.iproject.febricook.login.pref

import android.content.Context
import android.preference.PreferenceManager

class PreferencesHelper(context: Context) {
    companion object {
        private const val DEVICE_TOKEN = ""
    }

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    // save device token
    var deviceToken = preferences.getString(DEVICE_TOKEN, "")
        set(value) = preferences.edit().putString(DEVICE_TOKEN, value).apply()
}