package com.iproject.febricook.login.entity

class Promo(
    val id: Int,
    val imageUrl: String,
    val contentInfo: String
)