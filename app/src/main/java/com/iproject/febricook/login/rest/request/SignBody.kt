package com.iproject.febricook.login.rest.request

data class SignBody(
    var email: String = "",
    var name: String = "",
    var password: String = ""
)