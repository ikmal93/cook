package com.iproject.febricook.login.menu.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iproject.febricook.R
import com.iproject.febricook.login.rest.response.FoodListResponse

class MenuAdapter(private val foodList: List<FoodListResponse>) : RecyclerView.Adapter<MenuViewHolder>() {
    interface OnItemClickListener {
        fun onDetailClick(position: Int)
    }

    private var itemListener: OnItemClickListener? = null

    fun setOnClickListener(listener: OnItemClickListener) {
        itemListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.menu_list, parent, false)
        return MenuViewHolder(view, itemListener)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.bindItem(foodList[position])
    }

    override fun getItemCount(): Int {
        return foodList.size
    }

}