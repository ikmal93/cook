package com.iproject.febricook.login.profil

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.iproject.febricook.R
import com.iproject.febricook.login.profil.adapter.ShowUserAdapter
import com.iproject.febricook.login.rest.response.ShowUserListResponse
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.show_user_list_activity.*
import retrofit2.Response

class ShowUserListActivity : AppCompatActivity() {

    private lateinit var showUserAdapter: ShowUserAdapter
    private var userListResponse: List<ShowUserListResponse> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_user_list_activity)
        setToolbar("User List")
        getUserList()
    }

    fun getUserList() {
        ApiClient.provideApiService().getUsers()
            .enqueue(object : retrofit2.Callback<List<ShowUserListResponse>> {
                override fun onFailure(call: retrofit2.Call<List<ShowUserListResponse>>, t: Throwable) {
                    Toast.makeText(
                        this@ShowUserListActivity,
                        "An error has occured, please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(
                    call: retrofit2.Call<List<ShowUserListResponse>>,
                    response: Response<List<ShowUserListResponse>>
                ) {
                    response.body()?.let {
                        userListResponse = it
                        user_list_recycler.addItemDecoration(DividerItemDecoration(this@ShowUserListActivity, DividerItemDecoration.VERTICAL))
                        user_list_recycler.itemAnimator = DefaultItemAnimator()
                        user_list_recycler.setHasFixedSize(true)
                        user_list_recycler.layoutManager = LinearLayoutManager(this@ShowUserListActivity)
                        showUserAdapter = ShowUserAdapter(it.toList())
                        user_list_recycler.adapter = showUserAdapter
                    }

                }
            })
    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(user_list_toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = titleToolbar
        }

        user_list_toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}
