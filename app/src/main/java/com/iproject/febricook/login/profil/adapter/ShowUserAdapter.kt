package com.iproject.febricook.login.profil.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iproject.febricook.R
import com.iproject.febricook.login.rest.response.ShowUserListResponse

class ShowUserAdapter(private val showUserList: List<ShowUserListResponse>) : RecyclerView.Adapter<ShowUserViewHolder>() {
    interface OnItemClickListener {
        fun onDetailClick(position: Int)
    }

    private var itemListener: OnItemClickListener? = null

    fun setOnClickListener(listener: OnItemClickListener) {
        itemListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowUserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.show_user_list_item, parent, false)
        return ShowUserViewHolder(view, itemListener)
    }

    override fun onBindViewHolder(holder: ShowUserViewHolder, position: Int) {
        holder.bindItem(showUserList[position])
    }

    override fun getItemCount(): Int {
        return showUserList.size

    }
}