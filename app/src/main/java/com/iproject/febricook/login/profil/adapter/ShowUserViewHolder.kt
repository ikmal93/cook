package com.iproject.febricook.login.profil.adapter

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.iproject.febricook.login.rest.response.ShowUserListResponse
import kotlinx.android.synthetic.main.show_user_list_item.view.*

class ShowUserViewHolder(itemView: View, itemListener: ShowUserAdapter.OnItemClickListener?): RecyclerView.ViewHolder(itemView){
    private val list: ConstraintLayout = itemView.show_user_list_constraint
    private val name: TextView = itemView.show_name
    private val email: TextView = itemView.show_email

    init {
        list.setOnClickListener {
            if (itemListener != null) {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemListener.onDetailClick(adapterPosition)
                }
            }
        }
    }

    fun bindItem(showUserListResponse: ShowUserListResponse){
        name.text = showUserListResponse.name
        email.text = showUserListResponse.email

    }
}