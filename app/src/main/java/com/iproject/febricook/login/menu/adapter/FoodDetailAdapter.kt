package com.iproject.febricook.login.menu.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.iproject.febricook.R
import com.iproject.febricook.login.rest.response.Image
import kotlinx.android.synthetic.main.banner_info.view.*

class FoodDetailAdapter (private var mContext: Context, private val infoBanner : List<Image>?): PagerAdapter() {

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return infoBanner!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val view : View = LayoutInflater.from(mContext).inflate(R.layout.banner_info, container, false)
        Glide
                .with(mContext)
                .load(infoBanner?.get(position)?.name)
                .centerCrop()
                .into(view.banner_info_image)
        container.addView(view, 0)
        view.setOnClickListener {

        }
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


}