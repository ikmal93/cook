package com.iproject.febricook.login.menu

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.iproject.febricook.R
import com.iproject.febricook.login.menu.adapter.FoodDetailAdapter
import com.iproject.febricook.login.rest.response.FoodListResponse
import kotlinx.android.synthetic.main.food_detail_activity.*
import java.util.*

class FoodDetailActivity : AppCompatActivity() {

    private var foodDetails: FoodListResponse? = null

    private var swipeTimer = Timer()
    private var isTimerRunning = true
    private var nextPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.food_detail_activity)
        val title = intent.getStringExtra("title")
        val food = intent.getStringExtra("foodList")
        foodDetails = Gson().fromJson(food, FoodListResponse::class.java)
        setToolbar(title)


        val imageAdapter = FoodDetailAdapter(this@FoodDetailActivity, foodDetails?.images)
        food_detail_pager.adapter = imageAdapter
        food_detail_tab.setupWithViewPager(food_detail_pager)
        setUpAutoSlider()

        food_detail_name_content.text = foodDetails?.title
        food_detail_province_content.text = foodDetails?.province_name
        food_detail_kekurangan_content.text = foodDetails?.disadvantages?.get(0)?.description
        food_detail_kelebihan_content.text = foodDetails?.benefits?.get(0)?.description


    }

    private fun setToolbar(titleToolbar: String) {
        setSupportActionBar(food_detail_toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = titleToolbar
        }

        food_detail_toolbar.setNavigationOnClickListener {
            finish()
        }
    }


    override fun onResume() {
        super.onResume()
        if (!isTimerRunning)
            setUpAutoSlider()
    }

    override fun onStop() {
        super.onStop()
        swipeTimer.cancel()
        isTimerRunning = false
    }

    private fun setUpAutoSlider() {
        val handler = Handler()
        val update = Runnable {
            nextPage = food_detail_pager.currentItem + 1
            if (nextPage == foodDetails?.images?.size) {
                nextPage = 0
            }
            food_detail_pager.setCurrentItem(nextPage, true)
        }

        swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 5000, 5000)
    }
}
