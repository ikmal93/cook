package com.iproject.febricook.login.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.iproject.febricook.R
import com.iproject.febricook.login.home.HomeActivity
import com.iproject.febricook.login.pref.PreferencesHelper
import com.iproject.febricook.login.rest.request.LoginBody
import com.iproject.febricook.login.rest.response.LoginResponse
import com.iproject.febricook.login.signup.SignUpActivity
import id.co.wmotion.savioryrots.service.ApiClient
import kotlinx.android.synthetic.main.login_activity.*
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    private var loginResponse: LoginResponse? = null
    private var preferencesHelper: PreferencesHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        preferencesHelper = PreferencesHelper(this)

//        login_username_content_tie.setText("roy@gmail.com")
//        login_password_content_tie.setText("12345")

        login_btn.setOnClickListener {
            val login = LoginBody()
            login.email = login_username_content_tie.text.toString()
            login.password = login_password_content_tie.text.toString()
            postLogin(login)
        }

        login_sign_up.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
    }

    fun postLogin(login: LoginBody) {
        ApiClient.provideApiService().login(login)
            .enqueue(object : retrofit2.Callback<LoginResponse> {
                override fun onFailure(call: retrofit2.Call<LoginResponse>, t: Throwable) {
                    Toast.makeText(this@LoginActivity, "An error has occured, please try again", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(call: retrofit2.Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.code() == 200) {
                        loginResponse = response.body()
                        preferencesHelper?.deviceToken = loginResponse?.token
                        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                    } else {
                        Toast.makeText(this@LoginActivity, "username or email not valid", Toast.LENGTH_SHORT).show()
                    }

                }
            })
    }
}
